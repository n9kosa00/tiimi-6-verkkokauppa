drop database if exists pcag;

create database pcag;

use pcag;

create table kategoria (
    id int primary key auto_increment,
    nimi varchar(255) not null
);

create table tuote (
    id int primary key auto_increment,
    nimi varchar(255) not null,
    kuvaus text not null,
    kuva varchar(255),
    hinta decimal(10,2),
    kategoria_id int not null,
    index (kategoria_id),
    foreign key (kategoria_id) references kategoria(id)
    on delete restrict
);

create table asiakas (
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    osoite varchar(50) not null,
    postinro varchar(5) not null,
    postitmp varchar(50) not null,
    email varchar(255) not null,
    puhelin int not null
);

create table tilaus (
    id int primary key auto_increment,
    asiakas_id int not null,
    tilausaika datetime default current_timestamp,
    tila enum ('tilattu', 'maksettu', 'toimitettu'),
    foreign key (asiakas_id) references asiakas(id)
    on delete restrict
);

create table tilausrivi (
    tilaus_id int not null,
    rivinro int not null,
    tuote_id int not null,
    kpl int,
    foreign key (tilaus_id) references tilaus(id),
    foreign key (tuote_id) references tuote(id)
    on delete restrict
);

create table user (
    id int primary key auto_increment,
    username varchar(30) not null unique,
    password varchar(255) not null
);

ALTER TABLE `tilausrivi`
  ADD UNIQUE KEY `tilaus_id` (`tilaus_id`,`tuote_id`),
  ADD KEY `tuote_id` (`tuote_id`);

############################### Kategorioiden lisäys #############################
INSERT INTO `kategoria` (`id`, `nimi`) VALUES ('1', 'Prosessorit');
INSERT INTO `kategoria` (`id`, `nimi`) VALUES ('2', 'Emolevyt');
INSERT INTO `kategoria` (`id`, `nimi`) VALUES ('3', 'Kiintolevyt/SSD');
INSERT INTO `kategoria` (`id`, `nimi`) VALUES ('4', 'Ram');
INSERT INTO `kategoria` (`id`, `nimi`) VALUES ('5', 'Näytönohjaimet');
INSERT INTO `kategoria` (`id`, `nimi`) VALUES ('6', 'Virtalähteet');



############################## Tuotteiden lisäys ################################
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('1', 'AMD Ryzen 5 3600, AM4, 3.6 GHz, 6-core', '3. sukupolven Ryzen on viimein täällä ja tuo mukanaan tuen PCIe 4.0 -väylälle!\r\n\r\nTekniset tiedot:\r\n\r\nMalli: Ryzen 5 3600\r\nKanta: AMD AM4\r\nYdinten määrä: 6\r\nThreadien määrä: 12\r\nKellotaajuus:\r\n - Max Boost: 4.20 GHz\r\n - Base: 3.60 GHz\r\nVälimuisti:\r\n - L2: 3MB\r\n - L3: 32MB\r\nTDP: 65W\r\nJäähdytys: Wraith Stealth', 'Ryzen5_3600.jpg', '199', '1');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('2', 'AMD Ryzen 9 3900X, AM4, 3.8GHz, 12-core', '3. sukupolven Ryzen on viimein täällä ja tuo mukanaan tuen PCIe 4.0 -väylälle!\r\n\r\nTekniset tiedot:\r\n \r\nMalli: Ryzen 9 3900X\r\nKanta: AMD AM4\r\nYdinten määrä: 12\r\nThreadien määrä: 24\r\nKellotaajuus:\r\n - Max Boost: 4.60 GHz\r\n - Base: 3.80 GHz\r\n \r\nVälimuisti:\r\n - L2: 6MB\r\n - L3: 64MB\r\n\r\nTDP: 105W\r\nJäähdytys: Wraith Prism With RGB LED', 'Ryzen9_3900x.jpg', '499', '1');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('3', 'AMD Ryzen 5 2600, AM4, 3.4 GHz, 6-core', '2. sukupolven Ryzen on täällä!\r\nTekniset tiedot:\r\n\r\nMalli: Ryzen 5 2600\r\nKanta: AMD AM4\r\nYdinten määrä: 6\r\nThreadien määrä: 12\r\nKellotaajuus:\r\n - Max Boost: 3.90 GHz\r\n - Base: 3.40 GHz\r\nVälimuisti: 19MB\r\nTDP: 65W\r\nJäähdytys: Wraith Stealth', 'Ryzen5_2600.jpg', '139', '1');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('4', 'Intel Coffee Lake Core i5-9600K, LGA1151, 3.70 GHz, boxed', 'HUOM! Pakkaus ei sisällä prosessorin tuuletinta\r\n\r\nTekniset tiedot:\r\n\r\nMalli: Core i5-9600K\r\nYdinten määrä: 6\r\nThreadien määrä: 6\r\nKellotaajuus: 3.70 GHz\r\nMaks. turbotaajuus: 4.50 GHz\r\nVälimuisti: 9MB\r\nKanta: LGA1151\r\nTeknologia: 14nm\r\nTDP: 95W\r\nProsessorin näytönohjain: Intel UHD Graphics 630', 'Inteli5_9600k.jpg', '249', '1');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('5', 'Intel Coffee Lake Core i9-9900K, LGA1151, 3.60 GHz, boxed', 'Tekniset tiedot:\r\n\r\nMalli: Core i9-9900K\r\nYdinten määrä: 8\r\nThreadien määrä: 16\r\nKellotaajuus: 3.60 GHz\r\nMaks. turbotaajuus: 5.00 GHz\r\nVälimuisti: 16MB\r\nKanta: LGA1151\r\nTeknologia: 14nm\r\nTDP: 95W\r\nProsessorin näytönohjain: Intel UHD Graphics 630', 'Inteli9_9900k.jpg', '539', '1');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('6', 'Intel Coffee Lake Core i7-9700K, LGA1151, 3.60 GHz, boxed', 'Tekniset tiedot:\r\n\r\nMalli: Core i7-9700K\r\nYdinten määrä: 8\r\nThreadien määrä: 8\r\nKellotaajuus: 3.60 GHz\r\nMaks. turbotaajuus: 4.90 GHz\r\nVälimuisti: 12MB\r\nKanta: LGA1151\r\nTeknologia: 14nm\r\nTDP: 95W\r\nProsessorin näytönohjain: Intel UHD Graphics 630', 'Inteli7_9700k.jpg', '439', '1');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('17', 'MSI B450 TOMAHAWK MAX, ATX-emolevy', 'Prosessorituki:\r\nAMD AM4-kanta\r\n\r\nPiirisarja:\r\nAMD B450\r\n\r\nMuistituki:\r\n4 x DDR4 DIMM, maks. 64GB', 'MSI_tomahawk.jpg', '129', '2');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('18', 'Asus PRIME B450-PLUS, ATX-emolevy', 'Prosessorituki: \r\nAMD AM4-kanta\r\n\r\nPiirisarja:\r\nAMD B450\r\n\r\nMuistituki:\r\nDual Channel DDR4 -muistiarkkitehtuuri\r\n4 x DDR4 DIMM, maks. 64GB', 'Asus_prime.jpg', '104', '2');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('19', 'Asus ROG Strix B450-F GAMING, ATX-emolevy', 'Prosessorituki: \r\nAMD AM4-kanta\r\n\r\nPiirisarja:\r\nAMD B450\r\n\r\nMuistituki:\r\nDual Channel DDR4 -muistiarkkitehtuuri\r\n4 x DDR4 DIMM, maks. 64GB', 'Asus_rog.jpg', '129', '2');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('20', 'Asus ROG STRIX Z390-F GAMING, ATX-emolevy', 'Prosessorituki:\r\n9./8. sukupolven Intel Core, Pentium Gold ja Celeron prosessorit\r\nLGA1151\r\n\r\nPiirisarja:\r\nIntel Z390', 'Asus_rog_strix.jpg', '199', '2');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('31', 'Samsung 1TB 860 QVO, 2.5\" SSD-levy, SATA III, MLC, 550/520 MB/S', 'Ominaisuudet:\r\n - Huippulaadukas SSD\r\n - Todellista luotettavuutta\r\n - Teratavuja tallennukseen\r\n - Älykästä yhteensopivuutta', 'Samsung_860.jpg', '129', '3');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('32', 'Samsung 1TB 970 EVO Plus SSD-levy, M.2 2280, PCIe 3.0 x4, NVMe, 3500/3300 MB/s', 'Kapasiteetti: 1000GB\r\nVaiheittainen lukunopeus: Jopa 3500 MB/s\r\nVaiheittainen kirjoitusnopeus: Jopa 3300 MB/s', 'Samsung_970.jpg', '239', '3');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('33', 'Seagate 2TB BarraCuda, sisäinen 3.5\" kiintolevy, SATA III, 7200rpm, 256MB', 'Laitteen tyyppi: Kiintolevyasema, sisäinen\r\nKapasiteetti: 2TB\r\nForm Factor: 3.5\"\r\nLiitäntä: SATA 6Gb/s\r\nPuskurin koko: 256MB\r\nNopeus: 7200 rpm', 'Seagate_barracuda.jpg', '72', '3');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('34', 'Kingston 480GB A400 SSD-levy, 2,5\", SATA III, 500/450MB/s, Stand-alone', 'Form Factor: 2.5\"\r\nLiitäntä: SATA Rev. 3.0 (6Gb/s) - taaksepäin yhteensopiva SATA Rev. 2.0 (3Gb/s)\r\nKapasiteetti: 480GB\r\nOhjain: 2Ch3\r\nNAND: TLC\r\nTiedonsiirtonopeus (ATTO):\r\n - Luku: Jopa 500 MB/s\r\n - Kirjoitus: Jopa 450 MB/s\r\n', 'Kingston_A400.jpg', '76', '3');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('41', 'Kingston 16GB (2 x 8GB) HyperX Fury, DDR4 3200MHz, CL16, 1.35V, musta', 'Kapasiteetti: 16GB (2 x 8GB)\r\nMuistityyppi: DDR4\r\nNopeus: 3200MHz (PC4-25600)\r\nJännite: 1.35V\r\nOminaisuudet: Single Rank, On-Die Termination (ODT), Intel Extreme Memory Profiles (XMP 2.0), puskuroimaton', 'Kingston_hyperx.jpg', '102', '4');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('42', 'Corsair 16GB (2 x 8GB) Vengeance LPX, DDR4 3200MHz, CL16, 1.35V', 'Kapasiteetti: 16GB (2 x 8GB)\r\nNopeus: 3200 MHz DDR4 CL16 (16-18-18-36)\r\nOminaisuudet: XMP\r\nJännite: 1.35V\r\nPinnien määrä: 288', 'Corsair_vengeance.jpg', '94', '4');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('43', 'Corsair 16GB (2 x 8GB) Vengeance RGB PRO, DDR4 3200MHz, CL16, 1.35V, musta', 'Kapasiteetti: 16GB (2 x 8GB)\r\nMuistityyppi: DDR4 DIMM 288-pin\r\nNopeus: 3200MHz (PC4-25600)\r\nLatenssi: CL16 (16-18-18-36)\r\nJännite: 1.35 V\r\nOminaisuudet: Dynaaminen Multi-Zone RGB-valaisu, XMP 2.0 -tuki\r\nOhjelmistolla hallinta: Corsair iCUE', 'Corsair_RGB.jpg', '109', '4');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('51', 'Asus GeForce RTX 2070 STRIX O8G GAMING -näytönohjain, 8GB GDDR6', 'GPU\r\nGPU: NVIDIA GeForce RTX 2070\r\nCUDA coret: 2304\r\nKellotaajuus:\r\n - OC Mode: 1845 MHz (Boost) / 1410 MHz (Base)\r\n - Gaming Mode: 1815 MHz (Boost) / 1410 MHz (Base)', 'ASUS_RTX_2070.jpg', '519', '5');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('52', 'PowerColor Radeon RX 5700 XT Red Devil OC -näytönohjain, 8GB GDDR6', 'GPU: AMD Radeon RX 5700 XT\r\nKellotaajuus:\r\n - Boost: Maks. 2010 MHz\r\n - Game: 1905 MHz\r\n - Base: 1770 MHz\r\n\r\nCoret: 2560 (40 CU)', 'Radeon_RX_5700.jpg', '469', '5');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('53', 'Asus GeForce RTX 2060 DUAL OC EVO -näytönohjain, 6GB GDDR6', 'GPU\r\nGPU: GeForce RTX 2060\r\nKellotaajuus:\r\n - OC Mode: 1785 MHz (Boost) / 1395 MHz (Base)\r\n - Gaming Mode: 1755 MHz (Boost) / 1365 MHz (Base)\r\n\r\nCUDA coret: 1920', 'ASUS_RTX_2060.jpg', '389', '5');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('54', 'Gigabyte Radeon RX 580 GAMING -näytönohjain, 8GB GDDR5', 'GPU\r\nGPU: AMD Radeon RX 580\r\nGPU:n kellotaajuus:\r\n - OC Mode: 1355 MHz\r\n - Gaming Mode: 1340 MHz ', 'Radeon_RX_580.jpg', '269', '5');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('55', 'Sapphire Radeon RX 5700 XT Pulse -näytönohjain, 8GB GDDR6', 'GPU: Radeon RX 5700 XT\r\nStream Processors: 2560\r\nCompute Units: 40\r\nKellotaajuus:\r\n - Base Clock: 1670 MHz\r\n - Game Clock: 1815 MHz\r\n - Boost Clock: Enintään 1925 MHz\r\n', 'Radeon_RX_5700_XT.jpg', '439', '5');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('56', 'Asus Radeon RX570 ROG Strix OC Edition -näytönohjain, 8GB GDDR5', 'GPU\r\nGPU: AMD Radeon RX 570\r\nKellotaajuus:\r\n - OC mode: 1310 MHz (Boost) / 1178 MHz (Base)\r\n - Gaming mode (Default): 1300 MHz (Boost) / 1168 MHz (Base)\r\n\r\nStream prosessorit: 2048', 'Radeon_RX570.jpg', '154', '5');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('61', 'Corsair 750W RM750x (2018), modulaarinen virtalähde, 80 Plus Gold', 'Tekniset tiedot:\r\nTäysmodulaarinen\r\nTeho: 750W\r\nHyötysuhde: 90%\r\nMitat: 160 x 150 x 86mm\r\nPaino: 1,66kg', 'Corsair_RM750x.jpg', '129', '6');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('62', 'Corsair 650W CV650 ATX-virtalähde, 80 PLUS Bronze, musta', 'Ominaisuudet:\r\n - 80 PLUS Bronze -sertifioitu\r\n - Täyttä jatkuvaa tehoa\r\n - Hiljainen jäähdytys\r\n - Kompakti kotelointi\r\n - Musta sukitus ja kotelointi\r\n - 3 vuoden takuu', 'Corsair_CV650.jpg', '65', '6');
INSERT INTO `tuote` (`id`, `nimi`, `kuvaus`, `kuva`, `hinta`, `kategoria_id`) VALUES ('63', 'Seasonic 650W FOCUS GX-650, modulaarinen ATX-virtalähde, 80 Plus Gold, musta', 'Ominaisuudet:\r\n - 80 PLUS Gold -sertifioitu\r\n - Kompakti koko - 140mm syvä\r\n - Tiukka jännitteen säätely\r\n - Cable-free Connection Design\r\n - S3FC - Tuulettimeton kunnes 30% kuormitus\r\n - Useamman näytönohjaimen tuki\r\n - Kullatut liittimet\r\n - 10 vuoden takuu*', 'Seasonic_GX-650.jpg', '99', '6');



