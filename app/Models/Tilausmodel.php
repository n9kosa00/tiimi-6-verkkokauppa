<?php namespace App\Models;

use CodeIgniter\Model;

use App\Models\AsiakasModel;
use App\Models\TilausriviModel;

class TilausModel extends Model{
    protected $table = 'Tilaus';
    
    protected $allowedFields = ['asiakas_id', 'tila'];

    public function tallenna($asiakas, $ostoskori){

        $this->db->transStart();
       
        $asiakas_id = $this->tallennaAsiakas($asiakas);
        

        $tilaus_id = $this->tallennaTilaus($asiakas_id);
        
        $this->tallennaTilausrivit($tilaus_id, $ostoskori);

        $this->db->transComplete();

       // if($this->db->transStatus() === FALSE){
            //rollback
        //}
    }
    private function tallennaAsiakas($asiakas){
        $asiakasModel = new AsiakasModel();
        $asiakasModel->save($asiakas);
        return $this->insertID();

    }
    private function tallennaTilaus($asiakas_id){
        $this->save([
            'asiakas_id'=> $asiakas_id,
            'tila' => 'tilattu'
        ]);

        return $this->insertID();

    }
    private function tallennaTilausrivit($tilaus_id, $ostoskori){
        $tilausriviModel =new TilausriviModel;
        $rivinro = 1;
        
        $db      = \Config\Database::connect();
        $builder = $db->table('tilausrivi');
        
        foreach ($ostoskori as $tuote_id){
            $db->query("INSERT INTO tilausrivi (tilaus_id,tuote_id,rivinro,kpl) VALUES ($tilaus_id,$tuote_id,$rivinro,1)
            ON DUPLICATE KEY UPDATE kpl=kpl+1");
           /* $tilausriviModel->save([
                'tilaus_id' => $tilaus_id,
                'tuote_id' => $tuote_id,
                'rivinro'=> $rivinro++,
                'kpl' => 1
            ]);*/
        }
    }
}