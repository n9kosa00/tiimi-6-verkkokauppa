<?php namespace App\Models;

use CodeIgniter\Model;

class TuoteModel extends Model{

    protected $table = 'tuote';
    protected $primarykey = 'id';
    protected $allowedFields = ['nimi', 'kuvaus','hinta','kuva','kategoria_id'];
    
    public function haeTuotteet($idt) {
        $palautus = array();
        foreach ($idt as $id) {
          $this->table('tuote');
          $this->select('id,nimi,hinta');
          $this->where('id',$id);
          $query = $this->get();
          $tuote = $query->getRowArray();
          $this->lisaaTuoteTaulukkoon($tuote,$palautus);
            
          $this->resetQuery();
        }
       
        return $palautus;
      }
      
      public function tuotehaku($key) {
        $this->table('tuote');
        $this->select('*');
        $this->like('nimi', $key);
        $query = $this->get();

        return $query->getResultArray();

      }
      private function lisaaTuoteTaulukkoon($tuote,&$taulukko) {
        for ($i = 0;$i < count($taulukko);$i++ ) {
          if ($taulukko[$i]['id'] === $tuote['id']) {
            $taulukko[$i]['maara'] = $taulukko[$i]['maara']  + 1;
            return;
          }
        }
        $tuote['maara'] = 1; // Tuote ei ollut taulukossa, joten asetetaan määräksi 1.
        array_push($taulukko,$tuote);
      }
    
    public function random($kat) {
        
        $this->table('tuote');
        $this->select('*');
        $this->where('kategoria_id', $kat);
        $this->order_by('rand()');
        $this->limit('4');
        
        $query = $this->get();

        return $query->getResultArray();
    }
  
}
