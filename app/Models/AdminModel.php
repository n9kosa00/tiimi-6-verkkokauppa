<?php namespace App\Models;

use CodeIgniter\Model;

Class AdminModel extends Model{
    protected $table = 'tuote';

    protected $allowedFields = ['nimi', 'kuvaus','hinta','kuva','kategoria_id'];

    public function getTuote(){
        return $this->findAll();
    }
}