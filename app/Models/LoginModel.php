<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model{

    protected $table= 'user';

    protected $allowedFields = ['username','password'];

    public function check($username,$password) {
        $this->where('username', $username);
        $query = $this->get();

        $row = $query->getRow();
        if ($row) {
            if (password_verify($password,$row->password)) {
                return $row;
            }
            //nykyinen admin salasana ei ole hassatty poistettaan kun admin salasana saadaan hassattyä.
            //if($password==$row->password){
               // return "admin";
            //}
        }
        return null;
    }
}
?>