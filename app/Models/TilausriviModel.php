<?php namespace App\Models;

use CodeIgniter\Model;



class TilausriviModel extends Model{
    protected $table = 'tilausrivi';

    protected $allowedFields = ['tilaus_id', 'rivinro','tuote_id','kpl'];
}