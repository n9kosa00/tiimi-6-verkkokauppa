<?php namespace App\Models;

use CodeIgniter\Model;

Class KategoriaModel extends Model{
    protected $table = 'kategoria';

    protected $allowedFields = ['id', 'nimi'];

    public function getAll() {
        $this->table('kategoria');
        $this->select('id, nimi');
        $query = $this->get();

        return $query->getResultArray();
    }
}