<div class="container-md">
<div class="row">
    <div class="col p-4">
        
        <h3 class="mb-3">Kategoriat</h3>
        <div class="btn-group-vertical">
            
            <?php foreach ($lista as $kategoria): ?>
            
                <a class="btn btn-outline-info btn-lg" href="<?= site_url("selaa/selaaTuotteita/" . $kategoria['id'] . $kategoria['nimi'])?>"><?=$kategoria['nimi'];?></a>
               
            <?php endforeach;?>
         
            </div>
    </div>
</div>
</div>
