<div class="container px-5">
    <div class="row mt-3 mb-3">
      <div class="col-xl-8">
        <div class="btn-group btn-group flex-wrap">
          <?php foreach ($lista as $kategoria): ?>        
            <a class="btn btn-outline-primary rounded-0 border-0 mb-2" href="<?= site_url("selaa/selaaTuotteita/" . $kategoria['id'] . $kategoria['nimi'])?>"><?=$kategoria['nimi'];?></a>         
          <?php endforeach;?>
        </div>
      </div>

      <div class="col-xl-4">
        <form action="<?php echo base_url("selaa/search")?>" method="post">
          <div class="input-group"> 
            <input type="text" name="search" class="form-control mr-2" placeholder="Hae tuotteita">
            <input class="btn btn-outline-primary " type="submit" value="Hae" name="submit"/>
          </div>
        </form>
      </div>
    </div>
            
            
  <div class="row">
      <?php foreach ($tuotteet as $tuote): ?>
        <div class="col-md-3 pb-4">
      <div class="card h-100">
      <div class="card-body">
        <img class="img-fluid p-1" src="<?="/uploads/" . $tuote['kuva'];?>">
      </div>
        <div class="card-footer">
        <h5 class=""><a href="<?= site_url("selaa/selaaTuote/" . $tuote['id'])?>"><?=$tuote['nimi'];?></a> </h5>
          <button type="button" onclick="window.location.href = '/selaa/lisaaOstoskoriin?tuoteId=<?= $tuote['id'] ?>&kategoria_id=<?= $tuote['kategoria_id'] ?>';" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></button>
          <h4 class="float-right price"><?=$tuote['hinta'];?>€</h4>
        </div>
      </div>
      </div>
    <?php endforeach;?>
    </div>
      </div>
      </div>

