<div class="container">
<?php foreach ($tuotteet as $tuote): ?>
    <div class="row">
        <div class="col-12">
                <h2 class="my-4"><?= $tuote['nimi']?></h2>   
        </div>
    </div>    
    <div class="row">
        <div class="col-md-5">
            <img class="img-fluid mb-4" width="400px" src="<?="/uploads/" . $tuote['kuva']?>" alt="">
        </div>
        <div class="col-md-7">
            <button type="button" onclick="window.location.href = '/selaa/lisaaOstoskoriin?tuoteId=<?= $tuote['id'] ?>&kategoria_id=<?= $tuote['kategoria_id'] ?>';" class="mb-3 mr-3 btn btn-primary"><i class="fas fa-shopping-cart"></i> Lisää ostoskoriin</button>
            <h2 class="price d-inline mb-3"><?= $tuote['hinta']?>€</h2>
            <p class="text-justify"><?= $tuote['kuvaus']?></p>
            <p class="pt-1"><span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span> 4.0 stars</p>
        </div>
    </div>  
        
        

    
</div>
<?php endforeach;?>