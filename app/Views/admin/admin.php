<div class="container">
<div class="row">
    <div class="col-6 p-4">
    <div class="mb-2">
    <h4>Luo listauksia</h4>
    <a class="btn btn-primary" href="<?= site_url('admin/create')?>" role="button"><span><i class="fas fa-plus"></i></span> Uusi tuote</a>
    <a class="btn btn-primary" href="<?= site_url('admin/addcategory')?>" role="button"><span><i class="fas fa-plus"></i></span> Uusi kategoria</a>
    </div>
    <div>
    <h4>Muokkaa ja poista</h4>
    <a class="btn btn-primary" href="<?= site_url('admin/tuotteet')?>" role="button"><span><i class="far fa-edit"></i></span> Muokkaa tuotteita</a>
    </div>
    
    
    </div>

    <div class="col-6 p-4 bg-dark text-white">
        <h3 class="mb-4">Kategoriat</h3>
        <table class="table table-dark">
            <thead>
                <tr>
                    <th>ID</td>
                    <th>Nimi</td>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($kategoriat as $kategoria): ?>
            <tr>
                <td><?= $kategoria['id'] ?></td>
                <td><?= $kategoria['nimi'] ?></td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
</div>
