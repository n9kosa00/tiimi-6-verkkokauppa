<div class="container p-3">
    <form action="/admin/addcategory">
        <div class="col-12">
            <h3>Lisää kategoria</h3>
            <?= \Config\Services::validation()->listErrors();?>
                <div class="form-group">
                    <label for="nimi">Nimi</label>
                    <input class="form-control" name="nimi"
                    placeholder="Kategorian nimi"
                    maxlength="255">
                </div>
            <button class="btn btn-primary mr-2">Luo kategoria</button>
            <?= anchor('admin/index', 'Takaisin') ?>

    </form>
</div>