<div class="container p-3">
<h3>Muokkaa tuotetta</h3>
<form action="/admin/updateRivi/">
<div class="col-12">
<?= \Config\Services::validation()->listErrors();?>
</div>
<input class="form-control" name="id"
            value="<?=$id?>"
            maxlength="255" type="hidden">
    <div class="form-row">
            <div class="col-6">
            <label for="nimi">Nimi</label>
            <input class="form-control" name="nimi"
            value="<?=$nimi?>"
            maxlength="255">
        </div>

        <div class="col">
            <label for="hinta">Hinta</label>
            <input class="form-control" name="hinta"
            step=".01"
            type="number"
            value="<?= $hinta?>"
            maxlength="255">
        </div>

        <div class="col">
            <label for="kategoria_id">Kategoria</label>
            <select class="form-control" name="kategoria_id">
                <?php foreach ($kategoriat as $kategoria): ?>
                <option value="<?= $kategoria['id'] ?>"><?= $kategoria['id'] . '. ' . $kategoria['nimi'] ?></option>
                <?php endforeach;?>
            </select>
        </div>

    </div>
    
    <div class="form-group mt-3">
        <textarea class="form-control" name="kuvaus" value="<?= $kuvaus?>"
        maxlength="255"></textarea>
    </div>
    <div class="form-group">
        <label for="kuva">Kuva</label>
        <input class="form-control" name="kuva"
        type="file"
        value="<?= $kuva?>">
    </div>
  
    <button class="btn btn-primary mr-2">Tallenna tuote</button>
    <?= anchor('admin/index', 'Takaisin') ?>
</form>
</div>