<div class="container">
<div class="row">
    <div class="col p-4">
        <h3 class="mb-3">Muokkaa tuotteita</h3>
        <a class="btn btn-primary mb-3" href="<?= site_url('admin/create')?>" role="button"><span><i class="fas fa-plus"></i></span> Uusi tuote</a>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nimi</th>
                    <th>Asetukset</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($lista as $tuote): ?>
            <tr>
                <td><?=$tuote['id'];?></td>
                <td><?=$tuote['nimi'];?></td>
                <td>
                    <a class="btn btn-danger" href="<?= site_url("admin/delete/" . $tuote['id'])?>" role="button"><span><i class="far fa-trash-alt"></i></span> Poista</a>
                    <a class="btn btn-primary" href="<?= site_url("admin/update/" . $tuote['id'])?>" role="button"><span><i class="far fa-edit"></i></span> Muokkaa</a>
                </td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>
</div>
