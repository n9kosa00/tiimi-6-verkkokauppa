<div class="container">
<div class="row">  
  <div class="col-lg-6 pt-4">
    <h4>Ostoskori</h4>
    <?php
    $summa = 0;
    
    
    ?>
    <table class="table">
    <?php foreach ($tuotteet as $tuote): ?>
      <tr>
        <td>
          <?= $tuote['nimi']?>
        </td>
        <td>
          <?= $tuote['hinta'] . ' €'?>
        </td>
        <td>
        <?= $tuote['maara'] ?> 
        </td>
        <td>
          <a class="ostoskori_poista"href="<?= site_url('ostoskori/poista/' . $tuote['id'])?>">
            <i class="fas fa-minus-circle"></i>
          </a>
        </td>
      </tr>
      <?php
      $summa += $tuote['hinta'] * $tuote['maara'];
      ?>
    <?php endforeach;?>
    <tr>
      <td>Tilauksen hinta</td>
      <td><?php printf("%.2f €",$summa);?></td>
      <td></td>
      <td>   
        <a id="tyhjenna" href="<?= site_url('ostoskori/tyhjenna');?>">
          <i class="fas fa-trash"></i>
        </a>
      </td>
    </tr>
    </table>
  </div>
  <div class="col-lg-6 pt-4">
    <h4>Tilaajan tiedot</h4>
    <form action="<?= site_url('tilaus/tilaa')?>" method="post">
    <div class="form-row">
      <div class="col-6">
        <label>Etunimi</label>
        <input name="etunimi" maxlength="50" class="form-control">
      </div>
      <div class="col-6 pb-2">
        <label>Sukunimi</label>
        <input name="sukunimi" maxlength="100" class="form-control">
      </div>
    </div>
    <div class="form-row">
      <div class="col-6">
        <label>Sähköposti</label>
        <input name="email" type="email" maxlength="255" class="form-control">
      </div>
      <div class="col-6 pb-2">
        <label>Puhelin</label>
        <input name="puhelin" maxlength="20" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label>Lähiosoite</label>
      <input name="osoite" maxlength="100" class="form-control">
    </div>
    <div class="form-row">
      <div class="col-4">
        <label>Postinumero</label>
        <input name="postinro" maxlength="5" class="form-control">
      </div>
      <div class="col-8 pb-2">
        <label>Postitoimipaikka</label>
        <input name="postitmp" maxlength="100" class="form-control">
      </div>
    </div>
      <button class="btn btn-primary">Tilaa</button>
    </form>
  </div>
</div>
</div>