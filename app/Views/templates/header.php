<?php
    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
?>
<html>
    <head>
        <title>PCAG verkkokauppa</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" 
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" 
              integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" 
              crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo base_url('/css/style.css'); ?>">
    </head>
    <body class="d-flex flex-column min-vh-100">
        <div class="container-fluid "> 
            <div class="row">
                <header class="col-12">
                    <nav class="navbar navbar-dark navbar-expand-md shadow">
                    <a class="navbar-brand" href="<?= site_url("etusivu/")?>">
                        <img src="/images/logo.png" height="40" class="d-inline-block align-top" alt="">
                    </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    <div class="collapse navbar-collapse" id="navbarNav">

                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?= site_url("etusivu/")?>">Etusivu<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= site_url("selaa/selaaTuotteita/1")?>">Tuotteet</a>
                            </li>                    
                        </ul>

                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                            <a class="nav-link" href="<?= site_url("ostoskori/")?>"><i class='fas fa-shopping-cart'></i> Ostoskori <span class="ml-2 badge badge-danger">
                            <?php 
                            if(isset($_SESSION['ostoskori'])) {
                                print count($_SESSION['ostoskori']);
                            } else {
                                print '0';
                            }
                            ?></span></a>
                            </li>
                        </ul>
                    </div>
                    </nav>
                </header>
            </div>
        </div>