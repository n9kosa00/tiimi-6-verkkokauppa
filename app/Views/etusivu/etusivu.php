<div class="position-relative overflow-hidden p-3 p-lg-5 m-lg-3 text-center bg-light" style="background-image: url('/images/bg_1.jpg'); background-size: 100% 100%;">
      <div class="col-lg-5 p-lg-5 mx-auto my-5 bg-light border border-info">
        <h1 class="display-4 font-weight-normal">Tervetuloa PCAG:n verkkokauppaan!</h1>
        <p class="lead font-weight-normal">Meidän sivustolta löydät uusimmista uusimmat ja tehokkaimmat komponentit, joka tarkoitukseen!</p>
        <a class="btn btn-outline-primary" href="<?= site_url("selaa/selaaTuotteita/1")?>">Katso tuotteita</a>
      </div>
      <div class="product-device box-shadow d-none d-md-block"></div>
      <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
    </div>
    <div class="container">
        <h3>Suosittelemme:</h3>
        <br>
        <h4 class="pb-2">Prosessorit</h4> 
    <div class="row">
  <?php foreach ($cpu as $tuote): ?>
    <div class="col-md-3 pb-5">
      <div class="card h-100">
      <div class="card-body">
        <img class="img-fluid p-1" src="<?="/uploads/" . $tuote['kuva'];?>">
      </div>
        <div class="card-footer">
        <h5 class=""><a href="<?= site_url("selaa/selaaTuote/" . $tuote['id'])?>"><?=$tuote['nimi'];?></a> </h5>
          <button type="button" onclick="window.location.href = '/selaa/lisaaOstoskoriin?tuoteId=<?= $tuote['id'] ?>&kategoria_id=<?= $tuote['kategoria_id'] ?>';" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></button>
          <h4 class="float-right price"><?=$tuote['hinta'];?>€</h4>
        </div>
      </div>
      </div>
    <?php endforeach;?>
  </div>
  <h4 class="pb-2">Näytönohjaimet</h4>
  <div class="row">
    
  <?php foreach ($gpu as $tuote): ?>
    <div class="col-md-3 pb-5">
      <div class="card h-100">
      <div class="card-body">
        <img class="img-fluid p-1" src="<?="/uploads/" . $tuote['kuva'];?>">
      </div>
        <div class="card-footer">
        <h5 class=""><a href="<?= site_url("selaa/selaaTuote/" . $tuote['id'])?>"><?=$tuote['nimi'];?></a> </h5>
          <button type="button" onclick="window.location.href = '/selaa/lisaaOstoskoriin?tuoteId=<?= $tuote['id'] ?>&kategoria_id=<?= $tuote['kategoria_id'] ?>';" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></button>
          <h4 class="float-right price"><?=$tuote['hinta'];?>€</h4>
        </div>
      </div>
      </div>
    <?php endforeach;?>
  </div>
  <h4 class="pb-2">Emolevyt</h4>
  <div class="row">
    
  <?php foreach ($mobo as $tuote): ?>
    <div class="col-md-3 pb-5">
      <div class="card h-100">
      <div class="card-body">
        <img class="img-fluid p-1" src="<?="/uploads/" . $tuote['kuva'];?>">
      </div>
        <div class="card-footer">
        <h5 class=""><a href="<?= site_url("selaa/selaaTuote/" . $tuote['id'])?>"><?=$tuote['nimi'];?></a> </h5>
          <button type="button" onclick="window.location.href = '/selaa/lisaaOstoskoriin?tuoteId=<?= $tuote['id'] ?>&kategoria_id=<?= $tuote['kategoria_id'] ?>';" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></button>
          <h4 class="float-right price"><?=$tuote['hinta'];?>€</h4>
        </div>
      </div>
      </div>
    <?php endforeach;?>
  </div>
</div>
    </div>
    </div>
    </div>