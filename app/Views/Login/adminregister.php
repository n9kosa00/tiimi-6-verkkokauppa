
<div class="container">

<h3>Register</h3>
<form action="/Login/registration">
<div class="col-12">
<?=\Config\Services::validation()->listErrors(); ?>
</div>
<div class="form-group">
<label>Username</label>
<input class="form-control" name="user" placeholder="Enter username" maxlength="30">
</div>
<div class="form-group">
<label>Password</label>
<input class="form-control" name="password" type="password" placeholder="Enter password" maxlength="30">
</div>
<div class="form-group">
<label>Password again</label>
<input class="form-control" name="confirmpassword" type="password" placeholder="Enter password again" maxlength="30">
</div>
<button class="btn btn-primary">Submit</button>
</form>
</div>