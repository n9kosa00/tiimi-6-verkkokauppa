<?php namespace App\Controllers;


use App\Models\Tilausmodel;
class Tilaus extends BaseController{

    public function __construct(){
        $session = \Config\Services::session();
        $session->start();
    }


    public function tilaa(){
        $asiakas = [
            'etunimi'=> $this->request->getPost('etunimi'),
            'sukunimi'=> $this->request->getPost('sukunimi'),
            'osoite'=> $this->request->getPost('osoite'),
            'postinro'=> $this->request->getPost('postinro'),
            'postitmp'=> $this->request->getPost('postitmp'),
            'email'=> $this->request->getPost('email'),
            'puhelin'=> $this->request->getPost('puhelin'),
        ];
        $tilausModel = new Tilausmodel();

        //tallennus
        $tilausModel->tallenna($asiakas, $_SESSION['ostoskori']);

        //tyhjennys
        unset($_SESSION['ostoskori']);

        //redirect
        return redirect()->to(site_url('tilaus/kiitos'));
        //$this->kiitos();
    }
    public function kiitos(){
            echo view('templates/header.php');
            echo view('tilaus/kiitostil.php');
            echo view('templates/footer.php');
    }
}