<?php namespace App\Controllers;

use App\Models\LoginModel;
class Login extends BaseController{
    public function __construct(){
        $session = \Config\Services::session();
        $session->start();
    }

public function index(){
    
        echo view('admin/adminheader'); 
        echo view('Login/login');
        echo view('admin/adminfooter');
}
public function adminregister(){
        

    
    echo view('admin/adminheader'); 
    echo view('Login/adminregister');
    echo view('admin/adminfooter');

}
public function registration(){
    $model = new LoginModel();
    if(!$this->validate([
        'user'=> 'required|min_length[8]|max_length[30]',
        'password' => 'required|min_length[8]|max_length[30]',
        'confirmpassword' => 'required|min_length[8]|max_length[30]|matches[password]',
    ])){
        echo view('admin/adminheader'); 
        echo view('Login/adminregister');
        echo view('admin/adminfooter');
    }
    else{
        $model->save([
            'username' => $this->request->getVar('user'),
            'password' => password_hash($this->request->getVar('password'),PASSWORD_DEFAULT),
        ]);
    }
    return redirect('login');
}
public function check(){
    $model = new LoginModel();

    if(!$this->validate([
        'user' => 'required|min_length[8]|max_length[30]',
        'password' => 'required|min_length[8]|max_length[30]'
    ])){
        echo view('admin/adminheader');
        echo view('Login/login');
        echo view('admin/adminfooter');

    }
    else{
        $user = $model->check(
            $this->request->getVar('user'),
            $this->request->getVar('password')
        );
        if ($user){
            $_SESSION['user'] = $user;
            return redirect('admin');
        }
        else{
            print ("kirjautuminen epäonnistui");
            print_r($user);
           // return redirect('login');
        }
    }
}
}
?>