<?php namespace App\Controllers;

use App\Models\AdminModel;
use App\Models\KategoriaModel;


class Admin extends BaseController
{

    public function __construct(){
        $session = \Config\Services::session();
        $session->start();
    }
    public function index(){
        if (!isset($_SESSION['user'])) {
            return redirect('login');
        }

        $katModel = new kategoriaModel();
        $data['kategoriat'] = $katModel->getAll();
        
        echo view('admin/adminheader.php');
        echo view('admin/admin.php',$data);
        echo view('admin/adminfooter.php');
        
}
        public function create(){
            if (!isset($_SESSION['user'])) {
                return redirect('login');
            }
            $model = new AdminModel();
            $katModel = new kategoriaModel();
            $data['kategoriat'] = $katModel->getAll();

            if(!$this->validate([
                'nimi' => 'required|max_length[255]',
                'kuvaus' => 'required|max_length[255]',
                'hinta' => 'required|decimal[10,2]',
                'kuva' => [
                    'uploaded[kuva]',
                    'mime_in[kuva,image/jpg,image/jpeg,image/gif,image/png]',
                    'max_size[kuva,4096]'
                ]
            ])){
                echo view('admin/adminheader.php');
                echo view('admin/create.php',$data);
                echo view('admin/adminfooter.php');
            } else {
                $kuva = $this->request->getFile('kuva');
                $polku = APPPATH;
                $polku = str_replace('app','public/uploads',$polku);
                $kuva->move($polku , $kuva->getName());
                $model->save([
                    'nimi' => $this->request->getVar('nimi'),
                    'kuvaus' => $this->request->getVar('kuvaus'),
                    'hinta' => $this->request->getVar('hinta'),
                    'kuva' => $kuva->getName(),
                    'kategoria_id' => $this->request->getVar('kategoria_id'),
                ]);
                
                return redirect('admin');
            }
        }

        public function addcategory(){
            if (!isset($_SESSION['user'])) {
                return redirect('login');
            }
            $model = new KategoriaModel();
            

            if(!$this->validate([
                'nimi' => 'required|max_length[255]'
            ])){
                echo view('admin/adminheader.php');
                echo view('admin/addcategory.php');
                echo view('admin/adminfooter.php');
            } else{
                $model->save([
                    'nimi' => $this->request->getVar('nimi')
                ]);
                return redirect('admin');
            }
        }
        public function tuotteet(){
            if (!isset($_SESSION['user'])) {
                return redirect('login');
            }
            $tuotteet_model = new AdminModel();
            $data['lista'] = $tuotteet_model->getTuote();
            echo view('admin/adminheader.php',$data);
            echo view('admin/tuotteet.php',$data);
            echo view('admin/adminfooter.php',$data);
        }
        public function delete($id){
            $tuotteet_model = new AdminModel();

            $tuotteet_model->delete($id);
            return redirect()->to(site_url('/admin/tuotteet'));

        }
        public function update($id){
            if (!isset($_SESSION['user'])) {
                return redirect('login');
            }
            $tuotteet_model = new AdminModel();
            $data = $tuotteet_model->find($id);
            
            $katModel = new kategoriaModel();
            $data['kategoriat'] = $katModel->getAll();

            echo view('admin/adminheader.php',$data);
            echo view('admin/muokkaa.php',$data);
            echo view('admin/adminfooter.php',$data);
        }
        public function updateRivi(){
            $tuotteet_model = new AdminModel();


            $id = $this->request->getVar('id');
            $nimi = $this->request->getVar('nimi');
            $kuvaus = $this->request->getVar('kuvaus');
            $kuva = $this->request->getVar('kuva');
            $hinta = $this->request->getVar('hinta');
            $kategoria_id = $this->request->getVar('kategoria_id');

            $data = [
                'nimi' => $nimi,
                'kuvaus' => $kuvaus,
                'kuva' => $kuva,
                'hinta' => $hinta,
                'kategoria_id' => $kategoria_id,
            ];
            /*print "id on $id";
            print_r ($data);
            exit;'*/            
            $tuotteet_model->update($id,$data);
            return redirect()->to(site_url('/admin/tuotteet'));
        }
        
}