<?php namespace App\Controllers;

use App\Models\TuoteModel;
class Etusivu extends BaseController
{
        public function __construct(){
                $session = \Config\Services::session();
                $session->start();
            }
        public function index()
        {

            $tuote_model = new TuoteModel();

            $data['gpu'] = $tuote_model->random('5');
            $data['cpu'] = $tuote_model->random('1');
            $data['mobo'] = $tuote_model->random('2');

                echo view('templates/header.php', $data);
                echo view('etusivu/etusivu.php', $data);
                echo view('templates/footer.php');
        }
}