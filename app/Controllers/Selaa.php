<?php namespace App\Controllers;

use App\Models\KategoriaModel;
use App\Models\TuoteModel;
class Selaa extends BaseController
{
    public function __construct(){
        $session = \Config\Services::session();
        $session->start();
    }
public function selaa(){
            $kategory_model = new KategoriaModel();
            $data['lista'] = $kategory_model->getAll();
            echo view('templates/header.php');
            echo view('selaa/selaa.php',$data);
            echo view('templates/footer.php');
        }

        public function selaaTuotteita($kategoriaId){

            if(!isset($_SESSION['ostoskori']))
             $_SESSION['ostoskori'] = [];


            $tuote_model = new TuoteModel();
            $kategory_model = new KategoriaModel();
            $data['lista'] = $kategory_model->getAll();
            $data['tuotteet'] = $tuote_model->where('kategoria_id', $kategoriaId)->findAll();
            


            echo view('templates/header.php');
            echo view('selaa/selaaTuotteita.php',$data);
            echo view('templates/footer.php');
        }
        public function selaaTuote($id){
            $tuote_model = new TuoteModel;

            $data['tuotteet'] = $tuote_model->where('id', $id)->findAll();
            echo view('templates/header.php');
            echo view('selaa/selaaTuote.php',$data);
            echo view('templates/footer.php');
        }
        public function search(){
            $key = $_POST["search"];
            $tuote_model = new TuoteModel;
            $kategory_model = new KategoriaModel();
            $data['lista'] = $kategory_model->getAll();
            $data['tuotteet'] = $tuote_model->tuoteHaku($key);
            echo view('templates/header.php');
            echo view('selaa/selaaTuotteita.php', $data);
            echo view('templates/footer.php');
        }
        

        public function lisaaOstoskoriin() {

            $tuoteId = $this->request->getVar("tuoteId");
            $kategoriaId= $this->request->getVar("kategoria_id");
            
            //print "$tuoteId $kategoriaId";
            array_push($_SESSION['ostoskori'], $tuoteId);

            

            return redirect()->to('selaaTuote/'.$tuoteId);
        }
    }