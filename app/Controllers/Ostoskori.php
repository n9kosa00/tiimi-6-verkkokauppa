<?php namespace App\Controllers;


//use App\Models\TuoteryhmaModel;
use App\Models\TuoteModel;

class Ostoskori extends BaseController
{
	//private $tuoteryhmaModel = null;
	private $tuoteModel = null;

	public function __construct() {
		$session = \Config\Services::session();
		$session->start();
		if (!isset($_SESSION['ostoskori'])) {
			$_SESSION['ostoskori'] = array();
		}
		$this->tuoteModel = new TuoteModel();
	}

	public function index()
	{

		if (count($_SESSION['ostoskori']) > 0) {
			$tuotteet = $this->tuoteModel->haeTuotteet($_SESSION['ostoskori']);
			//$tuotteet = $this->tuoteModel->find($_SESSION['ostoskori']);
		}
		else {
			$tuotteet = array();
		}
		
    	$data['tuotteet'] = $tuotteet;
		echo view('templates/header' );
		echo view('ostoskori/ostoskori.php',$data);
		echo view('templates/footer');
	}

	public function lisaa($tuote_id) {
		array_push($_SESSION['kori'],$tuote_id);
	
		//return redirect('kauppa/tuote/' . $tuote_id);
		return redirect()->to(site_url('/kauppa/tuote/' . $tuote_id));	
	}
	
	public function poista($tuote_id) {
		// Käydään läpi taulukko lopusta päin ja poistetaan kaikki tuotteet.
		// Jos poistamista lähtee tekemään alusta päin ja poistaa toiston sisällä tuotteita, saa
		// index out of bounds virheen. Sama tuote voi olla moneen kertaan istuntomuuttujan taulukossa, 
		// joten koko taulukko pitää käydä läpi.
		for ($i = count($_SESSION['ostoskori'])-1; $i >= 0;$i--) {
			if ($_SESSION['ostoskori'][$i] === $tuote_id) {
				array_splice($_SESSION['ostoskori'], $i, 1);
				// Jos halutaan, että poistetaan vain yksi kappale,
				// lisätään return lause tähän.
				//return redirect('ostoskori');
			}
		}
		return redirect('ostoskori');
	}

  public function tyhjenna() {
	  	//$kategoria_id = $_SESSION['kategoria_id'];
		$_SESSION['ostoskori'] = null;
		
    return redirect()->to(site_url('ostoskori'));		
  }
  
}
